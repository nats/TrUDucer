# "session" tool

For creating new conversion rules or checking coverage and precision of existing
rules, there is this graphical tool available.

## Features

- displays percentage of sentences which where not converted, as well as
percentage of each relation to cause a sentence to not be converted
- [visualizes trees, the conversion process](#visualizing-trees) and [compares them with a validation tree](#validating)
- easy navigation to find trees, by [sorting](#grouping) and [filtering](#filtering) them
- [convert treebank in graphical interface and see results](#conversion)

## Usage

When the tool is started a list of the not converted trees grouped by
blocking relation is shown on the left hand side. If no trees are showing
up, probably your output directory is empty (the treebank is not converted?).


### Usage Information from the software itself:

```
usage: TrUDucer session [-h] [-t TRANSDUCER] [--treecount TREECOUNT] [--validation VALIDATION] input_dir output_dir

positional arguments:
  input_dir              The directory containing the CoNLL files to be converted.
  output_dir             The directory where the converted files should be generated.

optional arguments:
  -h, --help             show this help message and exit
  -t TRANSDUCER, --transducer TRANSDUCER
                         The file containing the transformation rules.
  --treecount TREECOUNT  how many trees of the treebank to convert ("0" to convert all)
  --validation VALIDATION
                         the directory containing a validation set of converted CoNLL files
```

## Conversion

To convert a treebank, a rule file has to be given. This can be done either when
starting the tool with the command line parameter ```TRANSDUCER``` or by clicking
on **set Transduer** in the **Transducer** menu.
Then you can click on **convert** in the **Conversion** menu.

## Grouping

By default, only not converted trees are listed and sorted by blocking relations.
To list all trees regardless of conversion result, the **remember rules used**
checkbox in the **Conversion** menu has to be checked and the treebank has to
be converted again.
Afterwards, a list with all the sentences grouped by the used rules is also
available.

## Visualizing Trees

by clicking on a tree in the filelike tree explorer on the left side, the tree
will be rendered on the right side. 

To see the original tree before conversion, you can click on **show original
tree** in the **Tree** menu or right click on a tree in the tree picker on the
left side and select **show original tree**.
If you have selected a rule file, you can step through the conversion process
with the **<** and **>** buttons.

## Visualizing Rules

You can also visualize rules. For doing so, the treebank needs to be converted
with the **remember rules used** checkbox checked at least once. Afterwards, you
can right click on the rule you want to visualize and click on **visualize rule**.
This will show a popup window with the matcher tree structure on top
and the replacement tree structure below. This will also show frontier nodes
in the matcher structure, which is usually only implicitly contained in the rule syntax.

## Validating

To check a converted treebank against a validation treebank, you need to set a
validation directory for the tool. This can be done either as a program argument
```validation``` or with **set validation directory** in the **File** menu.
Then you can click on **validate** in the **conversion** menu. You can now compare
the converted trees with validation trees in a compare window by clicking .

## Filtering

You can filter the trees for rules used or tree structure in the original and
generated trees. To do that, right click on **conversions by rule** and click
on the desired filter menu. That will open an input dialog. An empty input
will reset the filter.

The rule filter will always reset all filters. After filtering, only trees using
a rule containing the input phrase will be shown.

The structure filter will take a while for big treebanks. Be patient or use
a subset of the treebank. Afterwards, only trees where either a subtree of the
source tree structure or a subtree of the generated tree matches the input tree
structure will be shown.
Input needs to be given in the TrUDucer rule syntax.
