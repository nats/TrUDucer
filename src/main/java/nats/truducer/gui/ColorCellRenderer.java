package nats.truducer.gui;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import java.awt.*;

class ColorCellRenderer extends DefaultTreeCellRenderer {

    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value,
                                                  boolean sel, boolean exp, boolean leaf, int row, boolean hasFocus) {
        super.getTreeCellRendererComponent(tree, value, sel, exp, leaf, row, hasFocus);

        // Assuming you have a tree of Strings
        Object node = ((DefaultMutableTreeNode) value).getUserObject();

        // If the node has been manually annotated
        if (leaf && node instanceof ConversionResult && ((ConversionResult) node).hasValidation()) {
            // Paint the node in green
            setForeground(new Color(35, 165 ,35));
        }
        // If the node is a leaf and is not fully converted
        else if (leaf && node instanceof ConversionResult && !((ConversionResult) node).getSuccessful()) {
            // Paint the node in red
            setForeground(new Color(165, 35 ,35));
        }

        return this;
    }
}
