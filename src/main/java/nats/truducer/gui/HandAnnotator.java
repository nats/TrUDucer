package nats.truducer.gui;

import cz.ufal.udapi.core.Node;
import cz.ufal.udapi.core.Root;
import io.gitlab.nats.deptreeviz.*;
import nats.truducer.data.RedirectableParse;
import nats.truducer.io.TreeReaderWriter;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class HandAnnotator {

    public HandAnnotator(JFrame origframe, File validationFile, File file) {
        JFrame frame = new JFrame();

        Container pane = frame.getContentPane();
        pane.setLayout(new BorderLayout());

        // Scroll Pane containing the actual tree
        JScrollPane scrollPane = new JScrollPane();

        DepTree<RedirectableParse, SimpleWord> depTree = new DepTree<>();

        // why is the constructor protected??
        DepTreeBaseInteractor<RedirectableParse, SimpleWord> interactor
                = new DepTreeBaseInteractor<RedirectableParse, SimpleWord>() {
            {}
        };

        interactor._dt = depTree;

        interactor.addDTRefreshListener(hasChanged -> {
//            System.out.println(hasChanged);
            scrollPane.setViewportView(depTree.getNodesCanvas());
        });

        interactor.addPopupListener(new PopupListener() {
            int _index;

            @Override
            public void makePopup(PopupEvent popupEvent) {
                String chosen = null;
                while (chosen == null) {
                    chosen = JOptionPane.showInputDialog("label:");
                }
                popupEvent.getNode().setLabel(popupEvent.getLevel(), chosen);
                RedirectableParse p = depTree.getDecParse();
                p.getVerticesLabels().get("SYN")
                        .set(popupEvent.getNode().getIndex(),
                                chosen);
//                depTree.setDecParse(p);
                interactor.redraw(popupEvent.getNode(), popupEvent.getLevel(), true);
//                scrollPane.setViewportView(depTree.getNodesCanvas());
            }

            @Override
            public void setIndex(int i) {
                _index = i;
            }
        });

        Root tree = TreeReaderWriter.fileToTree(file);
        RedirectableParse p = new RedirectableParse(
                UdapiToDeptreeviz.getParse(tree));
        interactor._dt.setDecParse(p);
        interactor.draw(true);

        scrollPane.setViewportView(depTree.getNodesCanvas());
        int fontSize = depTree.getFont().getSize();
        scrollPane.getHorizontalScrollBar()
                .setUnitIncrement(fontSize * 3);
        scrollPane.getVerticalScrollBar().setUnitIncrement(fontSize * 3);
        scrollPane.setSize(depTree.getCanvasSize());

        pane.add(scrollPane, BorderLayout.NORTH);

        JButton cancel = new JButton("cancel");
        cancel.addActionListener(a -> {
            frame.dispose();
        });
        pane.add(cancel, BorderLayout.SOUTH);

        JButton save = new JButton("save");
        save.addActionListener(a -> {
            StringBuilder cs = new StringBuilder();
            cs.append("# sent_id = " + file.getName().substring(0, file.getName().indexOf(".")));
            cs.append("\n");
            tree.getComments().forEach(c -> {
                cs.append("#" + c);
                cs.append("\n");
            });
            for(int i = 0; i < p.getWords().size(); i++) {
                Node treenode = tree.getDescendants().get(i);
                // ord
                cs.append(treenode.getOrd());
                cs.append("\t");
                // form
                cs.append(treenode.getForm());
                cs.append("\t");
                // lemma
                cs.append(treenode.getLemma());
                cs.append("\t");
                // UPoS
                cs.append(treenode.getUpos());
                cs.append("\t");
                // PoS
                cs.append(treenode.getXpos());
                cs.append("\t");
                // features
                cs.append(treenode.getFeats());
                cs.append("\t");
                // head
                cs.append(p.getVerticesStructure().get("SYN").get(i) + 1);
                cs.append("\t");
                // deprel
                cs.append(p.getVerticesLabels().get("SYN").get(i));
                cs.append("\t");
                // ?
                cs.append("_\t");
                // misc
                cs.append(treenode.getMisc());
                cs.append("\n");
            }


            String r = cs.toString();
            System.out.println(r);
            try {
                FileWriter writer = new FileWriter(validationFile);
                writer.write(r);
                writer.close();
                FileWriter secondWriter = new FileWriter(file);
                secondWriter.write(r);
                secondWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            frame.dispose();
        });
        pane.add(save, BorderLayout.SOUTH);

        frame.pack();
        frame.setLocationRelativeTo(null); // center on screen
        frame.setVisible(true);
    }
}
