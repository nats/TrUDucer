package nats.truducer.gui;

import cz.ufal.udapi.core.Node;
import nats.truducer.interactive.InteractiveConversion;

import javax.swing.*;
import java.awt.*;

/**
 * Created by felix on 18/05/17.
 */
public class InteractiveConversionGUI implements InteractiveConversion {

    private final Frame frame;

    public InteractiveConversionGUI(Frame frame) {
        this.frame = frame;
    }

    @Override
    public String decideLabel(Node node, String... labels) {
        String chosen = null;
        while (chosen == null) {
            chosen = (String) JOptionPane.showInputDialog(frame, String.format("Decide the label for the Node '%s' (%s)", node.getForm(), node.getOrd()),
                    "Interactive Conversion", JOptionPane.PLAIN_MESSAGE, null, (Object[]) labels, labels[0]);
        }
        return chosen;
    }
}
