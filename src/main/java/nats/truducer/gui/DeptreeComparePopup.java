package nats.truducer.gui;

import cz.ufal.udapi.core.Node;
import cz.ufal.udapi.core.Root;
import io.gitlab.nats.deptreeviz.DepTree;
import io.gitlab.nats.deptreeviz.SimpleParse;
import io.gitlab.nats.deptreeviz.SimpleWord;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * compares two trees in a popup window. Differences are highlighted in color.
 * Main usage is to compare a validation tree with the conversion result,
 * but also used to display the before/after states of a rule visually.
 */
public class DeptreeComparePopup {

    public DeptreeComparePopup(Frame frame, Root beforeTree, Root afterTree) {
        JDialog dialog = new JDialog(frame);
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        JSlider zoomSlider = new JSlider(JSlider.HORIZONTAL, -15, 15, 0);

        // "before" panel, for validation the converted result
        JScrollPane scrollPane1 = new JScrollPane();
        scrollPane1.setPreferredSize(new Dimension(frame.getWidth() - 10, frame.getHeight()/2 - 40));
        DepTree<SimpleParse, SimpleWord> beforeTreeVis = new DepTree<>();
        SimpleParse p1 = UdapiToDeptreeviz.getParse(beforeTree);
        beforeTreeVis.setDecParse(p1);
        beforeTreeVis.draw(p1);
        beforeTreeVis.setHighlightColor(new Color(255, 0, 0));
        scrollPane1.setViewportView(beforeTreeVis.getNodesCanvas());
        panel.add(scrollPane1);

        // "after" panel, for validation the validation tree
        JScrollPane scrollPane2 = new JScrollPane();
        scrollPane2.setPreferredSize(new Dimension(frame.getWidth() - 10, frame.getHeight()/2 - 40));
        DepTree<SimpleParse, SimpleWord> afterTreeVis = new DepTree<>();
        SimpleParse p2 = UdapiToDeptreeviz.getParse(afterTree);
        afterTreeVis.setDecParse(p2);
        afterTreeVis.draw(p2);
        afterTreeVis.setHighlightColor(new Color(0, 200, 0));
        scrollPane2.setViewportView(afterTreeVis.getNodesCanvas());
        panel.add(scrollPane2);

        highlightChanges(beforeTree.getNode(), afterTree.getNode(), beforeTreeVis, afterTreeVis);

        zoomSlider.setPreferredSize(new Dimension(frame.getWidth() - 10, 30));
        zoomSlider.addChangeListener(a -> {
            beforeTreeVis.setZoomFactor(Math.pow(2.0, (((double) zoomSlider.getValue()) / 10.0)));
            beforeTreeVis.redraw();
            // size changed, set it again in the scrollPane to update
            scrollPane1.setViewportView(beforeTreeVis.getNodesCanvas());


            afterTreeVis.setZoomFactor(Math.pow(2.0, (((double) zoomSlider.getValue()) / 10.0)));
            afterTreeVis.redraw();
            // size changed, set it again in the scrollPane to update
            scrollPane2.setViewportView(afterTreeVis.getNodesCanvas());

            highlightChanges(beforeTree.getNode(), afterTree.getNode(), beforeTreeVis, afterTreeVis);
        });
        panel.add(zoomSlider);

        dialog.setContentPane(panel);
        dialog.pack();
        dialog.setLocationRelativeTo(null); // center on screen
        dialog.setVisible(true);
    }

    private void highlightChanges(Node beforeTree, Node afterTree, DepTree<SimpleParse, SimpleWord> beforeTreeVis, DepTree<SimpleParse, SimpleWord> afterTreeVis) {
        for(Node beforeChild: beforeTree.getChildren()) {
            Node afterChild = null;

            for(Node ac: afterTree.getChildren()) {
                if(beforeChild.getDeprel().equals(ac.getDeprel()) && beforeChild.getForm().equals(ac.getForm()) && beforeChild.getXpos().equals(ac.getXpos())) {
                    // equal child, this is correct!
                    afterChild = ac;
                    break;
                }
            }

            if(afterChild != null) {
                highlightChangesA(beforeChild, afterChild, beforeTreeVis, afterTreeVis);
            } else {
                markSubtree(beforeChild, beforeTreeVis);
            }
        }

        for(Node afterChild: afterTree.getChildren()) {
            Node beforeChild = null;

            for(Node bc: beforeTree.getChildren()) {
                if(afterChild.getDeprel().equals(bc.getDeprel()) && afterChild.getForm().equals(bc.getForm()) && afterChild.getXpos().equals(bc.getXpos())) {
                    // equal child, this is correct!
                    beforeChild = bc;
                    break;
                }
            }

            if(beforeChild!= null) {
                highlightChangesB(beforeChild, afterChild, beforeTreeVis, afterTreeVis);
            } else {
                markSubtree(afterChild, afterTreeVis);
            }
        }
    }

    private void highlightChangesB(Node beforeTree, Node afterTree, DepTree<SimpleParse, SimpleWord> beforeTreeVis, DepTree<SimpleParse, SimpleWord> afterTreeVis) {
        for(Node afterChild: afterTree.getChildren()) {
            Node beforeChild = null;

            for(Node bc: beforeTree.getChildren()) {
                if(afterChild.getDeprel().equals(bc.getDeprel()) && afterChild.getForm().equals(bc.getForm()) && afterChild.getXpos().equals(bc.getXpos())) {
                    // equal child, this is correct!
                    beforeChild = bc;
                    break;
                }
            }

            if(beforeChild!= null) {
                highlightChangesB(beforeChild, afterChild, beforeTreeVis, afterTreeVis);
            } else {
                markSubtree(afterChild, afterTreeVis);
            }
        }
    }

    private void highlightChangesA(Node beforeTree, Node afterTree, DepTree<SimpleParse, SimpleWord> beforeTreeVis, DepTree<SimpleParse, SimpleWord> afterTreeVis) {
        for(Node beforeChild: beforeTree.getChildren()) {
            Node afterChild = null;

            for(Node ac: afterTree.getChildren()) {
                if(beforeChild.getDeprel().equals(ac.getDeprel()) && beforeChild.getForm().equals(ac.getForm()) && beforeChild.getXpos().equals(ac.getXpos())) {
                    // equal child, this is correct!
                    afterChild = ac;
                    break;
                }
            }

            if(afterChild != null) {
                highlightChangesA(afterChild, beforeChild, beforeTreeVis, afterTreeVis);
            } else {
                markSubtree(beforeChild, beforeTreeVis);
            }
        }
    }

    private void markSubtree(Node afterTree, DepTree<SimpleParse, SimpleWord> afterTreeVis) {
        for(Node child: afterTree.getChildren()) {
            markSubtree(child, afterTreeVis);
        }
        int i = afterTree.getOrd() -1;
        afterTreeVis.getNode(i).setMarkedLevels(new ArrayList<String>(){{add("SYN");}});
        afterTreeVis.highlight(afterTreeVis.getNode(i));
    }
}
