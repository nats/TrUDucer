package nats.truducer.gui;

import javax.swing.*;
import java.awt.*;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * this shows a waiting cursor while some task is running.
 */
public class CursorManager {

    private static final Queue<BGTask> taskQueue = new ConcurrentLinkedQueue<>();
    private static final Lock lock = new ReentrantLock();

    public static void waitForTask(JFrame frame, BGTask bgTask) {

        // force cursor change!
        frame.toFront();
        frame.validate();
        frame.repaint();

        taskQueue.add(bgTask);

        new Thread(() -> {
            if(lock.tryLock()) {
                frame.setCursor(Cursor.WAIT_CURSOR);

                while(!taskQueue.isEmpty()) {
                    taskQueue.poll().apply();
                }

                frame.setCursor(Cursor.DEFAULT_CURSOR);
                lock.unlock();
            }
        }).start();


/*
        try {
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
        }
*/
    }
}

interface BGTask {
    void apply();
}
