package nats.truducer.data;

import io.gitlab.nats.deptreeviz.ParseInterface;
import io.gitlab.nats.deptreeviz.SimpleParse;
import io.gitlab.nats.deptreeviz.SimpleWord;

import java.util.*;

public class RedirectableParse implements ParseInterface<SimpleWord> {
    protected List<SimpleWord> words;
    protected List<String> levels = new ArrayList();
    protected Map<String, List<Integer>> structure;
    protected Map<String, List<String>> labels;

    public RedirectableParse(List<SimpleWord> words, List<Integer> heads, List<String> labels) {
        this.levels.add("SYN");
        this.words = words;
        this.structure = new HashMap();
        this.structure.put("SYN", heads);
        this.labels = new HashMap();
        this.labels.put("SYN", labels);
    }

    public RedirectableParse(SimpleParse parse) {
        words = new ArrayList<>();
        words.addAll(parse.getWords());
        levels.addAll(parse.getLevels());
        structure = new HashMap<>();
        labels = new HashMap<>();
        levels.forEach(s -> {
                    List<Integer> l = new ArrayList<>();
                    List<String> sl = new ArrayList<>();
                    l.addAll(parse.getVerticesStructure().get(s));
                    sl.addAll(parse.getVerticesLabels().get(s));
                    structure.put(s, l);
                    labels.put(s, sl);
                }
        );
    }

    public String print() {
        return null;
    }

    public List<SimpleWord> getWords() {
        return this.words;
    }

    public List<String> getLevels() {
        return this.levels;
    }

    public Map<String, List<Integer>> getVerticesStructure() {
        return this.structure;
    }

    public Map<String, List<String>> getVerticesLabels() {
        return this.labels;
    }

    public void redirectEdge(int pos, String level, int to) {
        structure.get(level).set(pos, to);
    }

    public static SimpleParse fromConll(List<String> conll) {
        List<Integer> heads = new ArrayList();
        List<String> labels = new ArrayList();
        List<SimpleWord> words = new ArrayList();
        Iterator var4 = conll.iterator();

        while(var4.hasNext()) {
            String line = (String)var4.next();
            String[] args = line.split("\t");
            if (args.length < 8) {
                break;
            }

            heads.add(Integer.parseInt(args[6]) - 1);
            labels.add(args[7]);
            words.add(new SimpleWord(args[1], args[4]));
        }

        return new SimpleParse(words, heads, labels);
    }
}
