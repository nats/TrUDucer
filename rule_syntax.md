# rule syntax  

### comments  
Comments start with a `#`  

### shorthands
you can define shorthands for groups of labels like this:  
`expansion predicatePos = [VAFIN, VVFIN, VVPP, VVINF, VVIZU, ADJD, ADJA];`  


## tree structure
the tree structure is represented as `element(subelem1, subelem2 ...)`  
and can be arbitrarily nested. The subelements are unordered!  
each element is a variable which has to occur on the right hand side.  

#### dependency labels
a variable can be restricted by its dependency label:  
`n:PRED` only matches elements with the PRED label  

#### Part of Speech tags  
You can also restrict matches by PoS:  
`n.VVFIN:KON` only matches nodes with PoS=VVFIN and label=KON  
Variables starting with `?` are catch-all variables that match all sub  
nodes not matched by other variables.  
  
#### match already converted nodes
you can match above the frontier (i.e. match already converted  
nodes) by explicitly setting the frontier with {} like below in  
S-Pred invert: the “parent” node is already translated and the  
current frontier is below the parent node.  If you don’t declare the  
position of the frontier, it is assumed to be above the root of the  
left-hand-side.  

#### variables as dependency labels
If you need to re-use a dependency label, you can match the label of  
a node with $var like this:  
`x:$label(....) -> y:$label`  
y will have the label x had before the translation.  this usually  
only makes sense for already translated nodes.  

## transducer rule
A full rule consists of two tree structures (the structure to match with
and the one to replace with) separated by a `->` followed with a `;`:  
`n:APP() -> n:compound();`
will try to match subtrees having a node with dependency relation "APP"
and produce a new structure with identical nodes but the dependency relation
"compound" instead, if such a match is found.

## groovy embeddings
You can add groovy code to restrict matching based on the current  
tree and the resulting tree:  
`p({n:APP()}) -> p(n:compound()) :- {n.getOrd() < p.getOrd()};`  
will only be applied if n is left of p. The variables are  
cz.ufal.udapi.core.Node Objects.  you can access the elements from  
the resulting tree by prefixing the variable name with an  
underscore, e.g. `_n` and `_p`.  The rule is only applied if the groovy  
code returns true.  You can change the data structures in groovy but  
be careful with that!  One use case would be to mark nodes for  
further manual inspection.